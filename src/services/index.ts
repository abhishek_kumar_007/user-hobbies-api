import UsersService from "./Users";
import HobbiesService from "./Hobbies";
import UsersModels from "../models/Users";
import HobbiesModels from "../models/Hobbies";
import {getMercavusDbConnector} from "../connectors";

(async () => {
    // connecting to mongoDB
    await getMercavusDbConnector().connect();
})();

const userModel = new UsersModels();
const hobbiesModel = new HobbiesModels();

userModel.initSchema();
hobbiesModel.initSchema();

const userModelInstance = userModel.getInstance();
const hobbiesModelInstance = hobbiesModel.getInstance();

export default () => {
    return {
        UserService: new UsersService({model: userModelInstance}),
        HobbiesService: new HobbiesService({model: hobbiesModelInstance}),
    }
};