import {QueryFilters} from "index";
import {User} from "../models/Users";
import BaseService, {BaseServiceOptions} from "./BaseService";

export interface UserServiceOptions extends BaseServiceOptions {
}

class UsersService extends BaseService {
    constructor(options: UserServiceOptions) {
        super(options);
    }

    getAllUsers = async (filters: QueryFilters) => {
        return this.getAll(filters);
    };

    addUser = (data: User) => {
        return this.insert(data)
    };
}

export default UsersService;