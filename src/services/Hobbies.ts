import {QueryFilters} from "index";
import {UsersHobby} from "../models/Hobbies";
import BaseService, {BaseServiceOptions} from "./BaseService";

export interface HobbiesServiceOptions extends BaseServiceOptions {
}

class HobbiesService extends BaseService {
    constructor(options: HobbiesServiceOptions) {
        super(options);
    }

    getAllHobbiesByUserId = async (userId: string, filters: QueryFilters) => {
        return this.getAll(filters, {userId});
    };

    addUserHobby = (data: UsersHobby) => {
        return this.insert(data)
    };

    deleteHobby = (hobbyId: string) => {
        return this.delete(hobbyId);
    };
}

export default HobbiesService;