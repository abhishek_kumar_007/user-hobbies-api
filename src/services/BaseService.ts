import mongoose, {Model, Types} from "mongoose";
import {BaseMongoDbFilters, QueryFilters} from "index";
import Logger from "../utils/logger";

export interface BaseServiceOptions {
    model: ReturnType<typeof mongoose.model>;
}

class BaseService {
    options: BaseServiceOptions;

    constructor(options: BaseServiceOptions) {
        this.options = options;
    }

    getAll = async (filters: QueryFilters, queryObject?: any) => {
        let {skip, limit, _id, ...others} = filters;

        skip = skip ? Number(skip) : 0;
        limit = limit ? Number(limit) : 10;

        const query: BaseMongoDbFilters = {
            ...others,
            ...queryObject
        };

        if (_id) {
            try {
                query._id = new mongoose.mongo.ObjectId(_id);
            } catch (error) {
                Logger.info("not able to generate mongoose id with content", _id);
            }
        }

        try {
            let items = await this.options.model
                .find(query)
                .skip(skip)
                .limit(limit);
            let total = await this.options.model.count(null);

            return {
                data: items,
            };
        } catch (errors) {
            Logger.error("unable to fetch", errors);
            throw new Error("unable to fetch" + errors.message);
        }
    };

    insert = async (data: any) => {
        try {
            let item = await this.options.model.create(data);
            if (item)
                return {
                    data: item
                };
        } catch (error) {
            Logger.error("unable to insert", error);
            throw new Error("unable to insert " + error.message);
        }
    };

    update = async (id: Types.ObjectId | string, data: any) => {
        try {
            let item = await this.options.model.findByIdAndUpdate(id, data, {new: true});
            return {
                item
            };
        } catch (error) {
            Logger.error("unable to update", error);
            throw new Error("unable to update " + error.message);
        }
    };

    delete = async (id: string) => {
        try {
            return await this.options.model.findByIdAndDelete(id);
        } catch (error) {
            Logger.error("unable to delete", error);
            throw new Error("unable to delete " + error.message);
        }
    }
}

export default BaseService;