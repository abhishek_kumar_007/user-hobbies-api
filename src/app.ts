import config from "config";
import {ApolloServer} from "apollo-server";
import {AppConfig} from "index";
import typeDefs from "./schemas";
import resolvers from "./resolvers";
import getServices from "./services";
import Logger from "./utils/logger/logger";
import {isProductionEnv} from "./utils/utils";

(async function () {
    try {
        let port = 3005; // default port
        const appConfig: AppConfig = config.get("app");

        // get port from config.
        if (appConfig && appConfig.port) {
            port = appConfig.port;
        }

        const server = new ApolloServer({
            typeDefs,
            resolvers,
            debug: !isProductionEnv(), // enabled only in dev env
            introspection: true,
            tracing: true,
            context: async ({req}: any) => {
                return {
                    request: req,
                    services: getServices()
                };
            },
            formatError: (error: Error) => {
                Logger.error("GraphQL error: " + error);
                return error;
            },
            formatResponse: (response: any, query: any) => {
                if (
                    query &&
                    query.context &&
                    query.context.request &&
                    query.context.request.body &&
                    query.context.request.body.operationName
                ) {
                    const info = JSON.stringify({
                        query: query.context.request.body.query,
                        variables: query.context.request.body.variables
                    });

                    Logger.info("GraphQL request: " + info);
                }
                return response;
            },
            onHealthCheck: () => {
                return new Promise((resolve, reject) => {
                    resolve();
                });
            },
        });

        server.listen({port: port}).then(({url}: { url: string }) => {
            Logger.info(
                `🚀 Server ready at http://localhost:${port}${server.graphqlPath}`
            );
            Logger.info(
                `Try your health check at: http://localhost:${port}/.well-known/apollo/server-health`
            );
        });

    } catch (e) {
        Logger.error("App initialization failed. " + e);
        process.exit(1);
    }
})();
