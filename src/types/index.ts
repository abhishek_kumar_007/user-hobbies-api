import {Types} from "mongoose";
import UsersService from "../services/Users";
import HobbiesService from "../services/Hobbies";
import {Context} from "apollo-server-core";

export interface AppConfig {
    port: number;
    dbConnectors: DbConnectorsConfig
}

export interface DbConnectorsConfig {
    mongoDB: MongoDatabaseConfig
}

export interface MongoDatabaseConfig {
    connectionURL: string
}

export interface QueryFilters {
    limit?: number;
    skip?: number;
    _id?: string;
}

export interface BaseMongoDbFilters {
    limit?: number;
    skip?: number;
    _id?: Types.ObjectId;
}

interface Services {
    services: {
        UserService: UsersService;
        HobbiesService: HobbiesService;
    }
}

export type ContextWithServices = Context<Services>;