// language=GraphQL
const Hobbies = `
    type UserHobbiesResponse {
        _id: ID!
        passionLevel: String!
        name: String!
        year: Int!
    }

    extend type Query {
        userHobbies(userId: ID!, filters: QueryFilters): [UserHobbiesResponse]
    }
`;


export default Hobbies;