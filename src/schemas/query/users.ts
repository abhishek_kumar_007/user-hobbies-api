// language=GraphQL
const Users = `
    type UsersResponse {
        _id: ID
        name: String
    }

    extend type Query {
        users(filters: QueryFilters): [UsersResponse]
    }
`;


export default Users;