// language=GraphQL
const Users = `
    
    input AddUserInput {
        name: String!
    }

    extend type Mutation {
        addUser(input: AddUserInput!): UsersResponse
    }
`;


export default Users;