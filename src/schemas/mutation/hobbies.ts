// language=GraphQL
const Hobbies = `

    type UserHobby {
        _id: ID!
        name: String!
        passionLevel: String!
        year: Int!
        userId: ID!
    }

    input AddUserHobbyInput {
        name: String!
        passionLevel: String!
        year: Int!
        userId: ID!
    }

    input DeleteUserHobbyInput {
        _id: ID
    }

    type DeleteHobbyResponse {
        success: Boolean
    }

    extend type Mutation {
        addUserHobby(input: AddUserHobbyInput!): UserHobby
        deleteHobby(input: DeleteUserHobbyInput!): DeleteHobbyResponse
    }
`;


export default Hobbies;