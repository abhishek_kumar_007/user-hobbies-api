import HobbiesQuery from "./query/hobbies";
import UsersQuery from "./query/users";
import HobbiesMutation from "./mutation/hobbies";
import UsersMutation from "./mutation/users";

// language=GraphQL
const RootQuery = `
    type Query {
        _blank : String
    }

    type Mutation {
        _blank : String
    }

    input QueryFilters {
        limit: Int
        skip: Int
        _id: ID
    }
`;

const typeDefs = [RootQuery, UsersQuery, HobbiesQuery, HobbiesMutation, UsersMutation];
export default typeDefs;
