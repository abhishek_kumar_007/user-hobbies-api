import config from "config";
import {MongoDatabaseConfig} from "index";
import MongoDbConnector from "./MongoDbConnector";

const DB_MERCAVUS = "mercavusChallenge";

export function getMercavusDbConnector() {
    const dbConfig: MongoDatabaseConfig = config.get("dbConnectors.mongoDB");
    const url = process.env.MONGO_DB_URL || `${dbConfig.connectionURL}/${DB_MERCAVUS}`;

    return new MongoDbConnector({
        url
    });
}