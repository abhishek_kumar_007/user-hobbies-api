import mongoose, {Schema} from "mongoose";
import Users from "./Users";

export interface UsersHobby {
    _id: string;
    passionLevel: string;
    name: string;
    year: number;
    userId: string
}

class Hobbies {
    modelName = "hobbies";

    initSchema() {
        const UsersInstance = new Users().getInstance();
        const schema = new Schema({
            passionLevel: {
                type: String,
                required: true,
            },
            name: {
                type: String,
                required: true,
            },
            year: {
                type: Number,
                required: true
            },
            userId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: UsersInstance
            },
        }, {timestamps: true});

        mongoose.model(this.modelName, schema);
    }

    getInstance() {
        return mongoose.model(this.modelName);
    }
}

export default Hobbies;