import mongoose, {Schema} from "mongoose";

export interface User {
    _id?: string;
    name: string;
}

class Users {
    modelName = "users";

    initSchema() {
        const schema = new Schema({
            name: {
                type: String,
                required: true,
            },
        }, {timestamps: true});

        mongoose.model(this.modelName, schema);
    }

    getInstance() {
        return mongoose.model(this.modelName);
    }
}

export default Users;