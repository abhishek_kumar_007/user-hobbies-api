import merge from "lodash/merge"
import UsersQueryResolver from "./query/users";
import HobbiesQueryResolver from "./query/hobbies";
import UsersMutationResolver from "./mutation/users";
import HobbiesMutationResolver from "./mutation/hobbies";

const mergedResolvers = [
    UsersMutationResolver,
    HobbiesMutationResolver,
    HobbiesQueryResolver,
    UsersQueryResolver
];

export default merge.apply(this, mergedResolvers);
