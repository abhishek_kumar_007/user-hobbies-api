import {User} from "../../models/Users";
import Logger from "../../utils/logger/logger";
import {ContextWithServices, QueryFilters} from "index";
import {GraphQlRuntimeException} from "../../exceptions";

interface UsersArgs {
    filters: QueryFilters;
}

interface UsersResponse extends User {
}

const UsersResolver = {
    Query: {
        users: async (parent: Object, args: UsersArgs, {services}: ContextWithServices): Promise<Array<UsersResponse>> => {
            try {
                const data = await services.UserService.getAllUsers(args.filters);
                return data.data;
            } catch (e) {
                Logger.error("Error in Users: " + e);
                throw new GraphQlRuntimeException("Error in Users", e);
            }
        },
    },
};

export default UsersResolver;
