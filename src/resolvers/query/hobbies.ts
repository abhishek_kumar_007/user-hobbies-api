import {ContextWithServices, QueryFilters} from "index";
import Logger from "../../utils/logger/logger";
import {UsersHobby} from "../../models/Hobbies";
import {GraphQlRuntimeException} from "../../exceptions";

interface UserHobbiesArgs {
    userId: string;
    filters?: QueryFilters | null;
}

interface UsersHobbiesResponse extends UsersHobby {
}

const HobbiesResolver = {
    Query: {
        userHobbies: async (parent: Object, args: UserHobbiesArgs, {services}: ContextWithServices): Promise<Array<UsersHobbiesResponse>> => {
            try {
                const data = await services.HobbiesService.getAllHobbiesByUserId(args.userId, args.filters || {});
                return data.data;
            } catch (e) {
                Logger.error("Error in HobbiesResolver:: unable to get user hobbies: " + e);
                throw new GraphQlRuntimeException("Error in HobbiesResolver", e);
            }
        },
    },
};

export default HobbiesResolver;
