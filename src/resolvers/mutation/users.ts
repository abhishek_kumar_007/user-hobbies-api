import {User} from "../../models/Users";
import {ContextWithServices} from "index";
import Logger from "../../utils/logger/logger";
import {GraphQlRuntimeException} from "../../exceptions";

interface AddUserArgs {
    name: string;
}

interface UsersInput {
    input: AddUserArgs
}

interface UsersResponse extends User {
}

const UsersResolver = {
    Mutation: {
        addUser: async (parent: Object, args: UsersInput, {services}: ContextWithServices): Promise<Array<UsersResponse>> => {
            try {
                const data = await services.UserService.addUser(args.input);
                return data.data;
            } catch (e) {
                Logger.error("Error in Users: " + e);
                throw new GraphQlRuntimeException("Error in Users", e);
            }
        },
    },
};

export default UsersResolver;
