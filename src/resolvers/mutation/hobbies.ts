import {ContextWithServices} from "index";
import Logger from "../../utils/logger/logger";
import {UsersHobby} from "../../models/Hobbies";
import {GraphQlRuntimeException} from "../../exceptions";

interface AddUserHobbyInput {
    input: UsersHobby
}

interface DeleteUserHobby {
    input: {
        _id: string
    }
}

interface AddUserHobbyResponse extends UsersHobby {
}

interface DeleteUserHobbyResponse {
    success: boolean
}

const HobbiesResolver = {
    Mutation: {
        addUserHobby: async (parent: Object, args: AddUserHobbyInput, {services}: ContextWithServices): Promise<Array<AddUserHobbyResponse>> => {
            try {
                const data = await services.HobbiesService.addUserHobby(args.input);
                return data.data;
            } catch (e) {
                Logger.error("Error in addUserHobby: " + e);
                throw new GraphQlRuntimeException("Error in addUserHobby", e);
            }
        },
        deleteHobby: async (parent: Object, args: DeleteUserHobby, {services}: ContextWithServices): Promise<DeleteUserHobbyResponse> => {
            try {
                await services.HobbiesService.deleteHobby(args.input._id);
                return {
                    success: true
                };
            } catch (e) {
                Logger.error("Error in deleteHobby: " + e);
                throw new GraphQlRuntimeException("Error in deleteHobby", e);
            }
        },
    },
};

export default HobbiesResolver;
