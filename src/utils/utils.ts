export const hasOwnProperty = (data: Object, field: string): boolean => {
    return Object.prototype.hasOwnProperty.call(data, field);
};

export const isProductionEnv = () => {
    return process.env.NODE_CONFIG_ENV === "production";
};
