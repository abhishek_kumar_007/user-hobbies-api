const Constants = {
    LOGS: {
        LOG_LEVELS: {
            DEBUG: "debug",
            INFO: "info",
            WARN: "warn",
            ERROR: "error"
        },
        DEFAULT_LOG_LEVEL: "debug"
    }
};

export default Constants;
