User Hobbies API
===============================

User Hobbies API is a graphql server which is a query language for API's to communicate.

Why GraphQL.?
---
GraphQL is a query language for APIs and a runtime for fulfilling those queries with your existing data. GraphQL provides a complete and understandable description of the data in your API, gives clients the power to ask for exactly what they need and nothing more, makes it easier to evolve APIs over time, and enables powerful developer tools.


Libraries
---------

* [GraphQL](https://graphql.org/)  
* [Apollo server & tools](https://www.apollographql.com/docs/apollo-server/)
* [Express](https://expressjs.com/)
* [TypeScript](https://www.typescriptlang.org/)
* [Winston](https://github.com/winstonjs/winston) Logging
* [Babel](https://babeljs.io/) ES6/ES7 to ES5 compiler
* [Eslint](https://eslint.org/) for syntax check
* [Jest](https://flow.org/en) and Enzyme for Unit testing

Design
---------

* **resolvers**     : 
  - Resolvers provide the instructions for turning a GraphQL operation (a query, mutation, or subscription) into data. 
    They either return the same type of data we specify in our schema or a promise for that data. 
  - It contains resolvers for each type in different directories.
    
* **Schema**      :
  - Contains graphql queries, mutations and subscriptions schemas.
  
* **types**         :
  - TypeScript type definitions
  
* **Utils**          : 
  - Utility classes. 

* **app.ts**        :
  - Server entry point and contains server init code.
               

Installation
----

#### Prerequisite

* You need to have [Node.js](https://nodejs.org/en/download/) (> 8) installed.
* [Yarn](https://yarnpkg.com/lang/en/docs/install/) for dependency management.

Usage
-----

#### How to run? ###
```shell script
Steps:
$ cd server # directory containing package.json
$ yarn install # if not done
$ yarn start # starts development server or
$ yarn start-prod # starts production server
```

This will start a server (using webpack).

Once you've started the server, navigate to http://localhost:3005/
to get started and play with playground!

#### How to run test suit? ###
```sh
Steps:
 $ cd server # directory containing package.json
 $ yarn install # if not done
 $ yarn test
```

It should print something like this:-
 
```
Test Suites: 3 passed, 3 total 
Tests:       4 passed, 4 total 
Snapshots:   0 total
Time:        1.783s
Ran all test suites.
```