import config from "config";
import gql from "graphql-tag";
import {createTestClient} from "apollo-server-testing";
import {ApolloServer} from "apollo-server";
import {AppConfig} from "index.ts";
import typeDefs from "../src/schemas";
import resolvers from "../src/resolvers";

it("Check if server crashes or not", async () => {

    let port = 3005; // default port
    const appConfig: AppConfig = config.get("app");

    // get port from config.
    if (appConfig && appConfig.port) {
        port = appConfig.port;
    }

    const GET_BLANK_QUERY = gql`
        query {
            _blank
        }
    `;

    const mocks = {
        String: () => "Hello",
    };

    const server = new ApolloServer({
        typeDefs,
        resolvers,
        mocks,
        context: async ({req}: any) => {
            return {
                request: req
            };
        }
    });

    // use the test server to create a query function
    const {query} = createTestClient(server);

    // run query against the server and snapshot the output
    const res = await query({query: GET_BLANK_QUERY});
    expect(res).toBeDefined();
    expect(res.data).toBeDefined();
    expect(res.data._blank).toBe("Hello");
    expect(res.errors).not.toBeDefined();
});